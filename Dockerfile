FROM python:3.9-slim

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt


# RUN chmod +x /app/

# RUN apk add --no-cache curl

# RUN python3 -m venv venv

ENV FLASK_APP=src/server.py

EXPOSE 5000

CMD ["flask","run","--host", "0.0.0.0"]